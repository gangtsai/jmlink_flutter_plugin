package cn.jiguang.jmlink_flutter_plugin;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import java.util.HashMap;
import java.util.Map;

import cn.jiguang.jmlinksdk.api.JMLinkAPI;
import cn.jiguang.jmlinksdk.api.JMLinkResponse;
import cn.jiguang.jmlinksdk.api.JMLinkResponseObj;
import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.view.FlutterNativeView;

public class JmlinkFlutterPlugin implements FlutterPlugin, MethodChannel.MethodCallHandler {

    private static final String TAG = "| JML | Android | -";

    public static JmlinkFlutterPlugin instance;
    private Context context;
    private MethodChannel channel;
    private static Uri myUri;
    private boolean isSetup = false;
    private boolean isRegisterDefaultHandler = false;


    @Override
    public void onAttachedToEngine(FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "com.jiguang.jmlink_flutter_plugin");
        channel.setMethodCallHandler(this);
        context = flutterPluginBinding.getApplicationContext();
    }


    @Override
    public void onDetachedFromEngine(FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
        instance.isSetup = false;
        instance.isRegisterDefaultHandler = false;
    }

    public JmlinkFlutterPlugin() {
        instance = this;
    }

    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        Log.d(TAG, "onMethodCall:" + call.method);
        if (call.method.equals("setup")) {
            setup(call, result);
        } else if (call.method.equals("registerJMLinkDefaultHandler")) {
            registerJMLinkDefaultHandler(call, result);
        }else {
            result.notImplemented();
        }
    }


    public static void setData(Uri uri) {
        Log.d(TAG, "setData:" + uri);
        myUri = uri;
        scheduleCache();
    }

    private void setup(MethodCall call, MethodChannel.Result result) {
        Log.d(TAG, "setup:" + call.arguments.toString());

        HashMap<String, Object> map = call.arguments();


        //初始化SDK
        JMLinkAPI.getInstance().init(context);

        //设置Debug
        Object debug = getValueByKey(call, "debug");
        if (debug != null) {
            JMLinkAPI.getInstance().setDebugMode((boolean) debug);
        }

        JmlinkFlutterPlugin.instance.isSetup = true;
        scheduleCache();
    }

    private void registerJMLinkDefaultHandler(MethodCall call, MethodChannel.Result result) {
        Log.d(TAG, "registerJMLinkDefaultHandler：");

        instance.isRegisterDefaultHandler = true;
        //首先注册回调接口
        JMLinkAPI.getInstance().register(new JMLinkResponse() {
            @Override
            public void response(JMLinkResponseObj obj) {
                //获取参数，进行后续工作
                Log.e("MainActivity", "replay = " + obj.paramMap + " " + obj.uri + " " + obj.source + " " + obj.type);
                HashMap<String, Object> hashMap = new HashMap<>();
                if (obj.paramMap!=null && !obj.paramMap.isEmpty()){
                    hashMap.put("paramMap", obj.paramMap);
                }
                if (obj.uri!=null && !"".equals(obj.uri)){
                    hashMap.put("uri", obj.uri.toString());
                }
                switch (obj.source){
                    case SCHEME:
                        hashMap.put("source", "SCHEME");
                        break;
                    case REPLAY:
                        hashMap.put("source", "REPLAY");
                        break;
                    case UNKNOWN:
                        hashMap.put("source", "UNKNOWN");
                        break;
                    default:
                        hashMap.put("source", "UNKNOWN");
                        break;
                }
                switch (obj.type){
                    case Installation:
                        hashMap.put("type", "Installation");
                        break;
                    case Open:
                        hashMap.put("type", "Open");
                        break;
                    case UNKNOWN:
                        hashMap.put("type", "UNKNOWN");
                        break;
                    default:
                        hashMap.put("type", "UNKNOWN");
                        break;
                }
                runMainThread(hashMap, null, "onReceiveJMLinkDefaultHandler");
            }
        });
        //然后再进行数据路由
//        Uri uri = getIntent().getData();
        // JMLinkAPI.getInstance().routerV2(myUri);
        scheduleCache();
    }

    public static void scheduleCache() {
        Log.d(TAG, "scheduleCache - " + "，isSetup=" + instance.isSetup + "，defaultHandler=" + instance.isRegisterDefaultHandler);
        if (instance == null || !instance.isSetup || !instance.isRegisterDefaultHandler) {
            return;
        }
        if (myUri != null) {
            router(myUri);
        }
    }

    private static void router(Uri uri) {
        Log.d(TAG, "router:" + uri);
        if (uri != null) {
            JMLinkAPI.getInstance().routerV2(uri);
            myUri = null;
        }
    }

    // 主线程再返回数据
    private void runMainThread(final HashMap<String, Object> map, final MethodChannel.Result result, final String method) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (result != null && method == null) {
                    result.success(map);
                } else if (method != null) {
                    channel.invokeMethod(method, map);
                } else {

                }

            }
        });
    }


    private Object valueForKey(Map para, String key) {
        if (para != null && para.containsKey(key)) {
            return para.get(key);
        } else {
            return null;
        }
    }

    private Object getValueByKey(MethodCall call, String key) {
        if (call != null && call.hasArgument(key)) {
            return call.argument(key);
        } else {
            return null;
        }
    }
}

