import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

typedef JMLDefaultHandlerListener = void Function(Map jsonMap);
typedef JMLHandlerListener = void Function(String key, Map jsonMap);

class JMLEventHandlers {
  static final JMLEventHandlers _instance = new JMLEventHandlers._internal();

  JMLEventHandlers._internal();

  factory JMLEventHandlers() => _instance;

  List<JMLDefaultHandlerListener> defaultHandlerEvents = [];
  Map<String, List<JMLHandlerListener>> handlerMap = Map();
}

class JmlinkFlutterPlugin {
  final String flutterLog = "| JML | Flutter | - ";

  static const String _methodChannelName = 'com.jiguang.jmlink_flutter_plugin';
  final MethodChannel _methodChannel;
  final JMLEventHandlers _eventHanders = new JMLEventHandlers();

  //工厂模式 : 单例公开访问点
  factory JmlinkFlutterPlugin() => _getInstance();

  static JmlinkFlutterPlugin get instance => _getInstance();

  //静态私有成员，没有初始化
  static JmlinkFlutterPlugin? _instance;

  //私有构造函数
  JmlinkFlutterPlugin._internal(MethodChannel channel)
      : _methodChannel = channel;

  //静态、同步、私有访问点
  static JmlinkFlutterPlugin _getInstance() {
    if (_instance == null) {
      _instance =
          new JmlinkFlutterPlugin._internal(MethodChannel(_methodChannelName));
    }
    return _instance!;
  }

  //添加默认的mLink handler的监听，在监听回调里，根据返回的参数做相应的操作，如：跳转页面
  addDefaultHandlerListener(JMLDefaultHandlerListener callback) {
    _eventHanders.defaultHandlerEvents.add(callback);
  }

  //监听回调方法
  Future<void> _handlerMethod(MethodCall call) async {
    print(flutterLog + "handleMethod method = ${call.method}");
    switch (call.method) {
      case 'onReceiveJMLinkDefaultHandler':
        {
          for (JMLDefaultHandlerListener cb
              in _eventHanders.defaultHandlerEvents) {
            Map jsonMap = call.arguments.cast<dynamic, dynamic>();
            cb(jsonMap);
          }
        }
        break;
      default:
        throw new UnsupportedError("Unrecognized Event");
    }
    return;
  }

  // 初始化SDK
  void setup({@required JMLConfig? config}) {
    print(flutterLog + "setup");
    _methodChannel.setMethodCallHandler(_handlerMethod);
    _methodChannel.invokeMethod("setup", config?.toMap());
  }

  //注册一个 mLink handler，当接收到URL，就会调用mLink handler，回调通过 addDefaultHandlerListener 来监听
  void registerJMLinkDefaultHandler() {
    print(flutterLog + "registerJMLinkDefaultHandler");
    _methodChannel.invokeMethod("registerJMLinkDefaultHandler");
  }

}

//配置类
class JMLConfig {
  String? appKey; // appKey 必须的,应用唯一的标识
  String? channel = ""; // channel 发布渠道. 可选，默认为空
  bool useIDFA = false; // only iOS， advertisingIdentifier 广告标识符（IDFA). 可选，默认为空
  bool isProduction =
      false; //isProduction 是否生产环境. 如果为开发状态,设置为NO;如果为生产状态,应改为YES.可选，默认为NO
  bool debug = false; //设置日志debug级别，可以查看更多日志

  JMLConfig() {
    print("JMConfig init");
  }

  JMLConfig.fromJson(Map<dynamic, dynamic> json)
      : appKey = json['appKey'],
        channel = json['channel'],
        useIDFA = json['useIDFA'],
        isProduction = json['isProduction'],
        debug = json['debug'];

  Map toMap() {
    return {
      'appKey': appKey,
      'channel': channel ??= "",
      'useIDFA': useIDFA,
      'isProduction': isProduction,
      'debug': debug,
    }..removeWhere((key, value) => value == null);
  }
}
